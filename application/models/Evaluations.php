<?php

class Evaluations extends CI_Model {

    var $table = 'evaluations';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function all($per_page = 10, $offset = 0, $count_all_results = false, $select = '') {
        if (!empty($select)) {
            $this->db->select($select);
        }
        $this->db->from($this->table);
        $this->db->order_by('created_at', 'desc');
        if ($count_all_results) {
            return $this->db->count_all_results();
        }
        if (!empty($per_page)) {
            $this->db->limit($per_page, $offset);
        }
        return $this->db->get()->result();
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($id = '') {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $find = $this->db->get($this->table);
        return $find->row();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function remove($id) {
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }

    public function is_available() {
        $this->db->where('status', 1);
        $this->db->from($this->table);
        return $this->db->get()->num_rows() > 0;
    }

    public function get_active_id() {
        $this->db->select('id');
        $this->db->where('status', 1);
        $this->db->from($this->table);
        $this->db->limit(1);
        $this->db->order_by('id', 'desc');
        $find = $this->db->get()->row();
        return ($find) ? $find->id : FALSE;
    }
    
    public function deactivate_all(){
        $this->db->set('status', 0);
        $this->db->where('status', 1);
        return $this->db->update($this->table);
    }

}
