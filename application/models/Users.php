<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model {

    var $table = 'users';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        //Do your magic here
    }

    public function valid_user($username = '', $password = '') {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->limit(1);
        $query = $this->db->get('users');
        return $query->num_rows() == 1 ? $query->row() : FALSE;
    }

    public function generate_password($email = '', $update = TRUE) {
        if (empty($email)) {
            return false;
        }
        $this->load->helper('string');
        $password = random_string('alnum', 4);
        if ($update) {
            $this->db->where('email', $email);
            $this->db->limit(1);
            $this->db->update('users', array('password' => md5($password)));
        }
        return $password;
    }

    public function email_exists($email) {
        $this->db->select('email');
        $this->db->where('email', $email);
        return $this->db->get('users')->num_rows() > 0 ? TRUE : FALSE;
    }
    
    public function is_exists($field, $value){
        $this->db->select($field);
        $this->db->where($field, $value);
        return $this->db->get('users')->num_rows() > 0 ? TRUE : FALSE;
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($user_id = '') {
        $this->db->where('id', $user_id);
        $this->db->limit(1);
        return $this->db->get($this->table)->row();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

}

/* End of file User.php */
/* Location: ./application/models/User.php */