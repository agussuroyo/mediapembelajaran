<?php

class Answers extends CI_Model {

    var $table = 'answers';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_all($per_page = 1, $offset = 0, $count_all_results = false) {
        $this->db->from($this->table);
        $this->db->order_by('created_at', 'DESC');
        if ($count_all_results) {
            return $this->db->count_all_results();
        }
        if ($per_page > 0) {
            $this->db->limit($per_page, $offset);
        }
        return $this->db->get()->result();
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($id = '') {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $find = $this->db->get($this->table);
        return $find->row();
    }

    public function find_by_question_id($id = '') {
        $this->db->where('question_id', $id);
        $this->db->order_by('lable', 'ASC');
        $find = $this->db->get($this->table);
        return $find->result();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function remove_by_question_id($id = '') {
        $this->db->where('question_id', $id);
        return $this->db->delete($this->table);
    }

}
