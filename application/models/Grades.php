<?php

class Grades extends CI_Model {

    var $table = 'grades';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function all($per_page = 0, $offset = 0, $count_all_results = false, $select = NULL) {
        if (!empty($select)) {
            $this->db->select($select);
        }
        $this->db->from($this->table . ' as g');
        if ($count_all_results) {
            return $this->db->count_all_results();
        }
        if ($per_page > 0) {
            $this->db->limit($per_page, $offset);
        }
        return $this->db->get()->result();
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($id = '') {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $find = $this->db->get($this->table);
        return $find->row();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function remove($id) {
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
    
    public function find_by_evaluation_id($id = ''){
        $this->db->select('g.*,u.username');
        $this->db->from($this->table . ' as g');
        $this->db->join('users as u', 'u.id = g.user_id');
        $this->db->where('g.evaluation_id', $id);
        $this->db->order_by('total', 'desc');
        $this->db->group_by('g.user_id');
        return $this->db->get()->result();
    }

}
