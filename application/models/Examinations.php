<?php

class Examinations extends CI_Model {

    var $table = 'examinations';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($id = '') {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $find = $this->db->get($this->table);
        return $find->row();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function remove($id) {
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
    
    public function find_by_evaluation_id($id){
        $this->db->where('evaluation_id', $id);
        $find = $this->db->get($this->table);
        return $find->result();
    }
    
    public function remove_by_evaluation_id($id){
        $this->db->where('evaluation_id', $id);
        return $this->db->delete($this->table);
    }

}
