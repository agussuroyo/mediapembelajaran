<?php

class Questions extends CI_Model {

    var $table = 'questions';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_all($per_page = 10, $offset = 0, $count_all_results = false, $select = null) {
        if (!empty($select)) {
            $this->db->select($select);
        }
        $this->db->from($this->table);
        $this->db->order_by('created_at', 'DESC');
        if ($count_all_results) {
            return $this->db->count_all_results();
        }
        if ($per_page > 0) {
            $this->db->limit($per_page, $offset);
        }
        return $this->db->get()->result();
    }

    public function get_keys() {
        $this->db->select('q.id as question, a.id as answer, q.weight');
        $this->db->from($this->table . ' as q');
        $this->db->join('answers as a', 'a.question_id = q.id AND a.key = 1', 'left');
        $this->db->group_by('q.id');
        $search = $this->db->get();
        return $search->result();
    }

    public function add($data = array()) {
        $save = $this->db->insert($this->table, $data);
        return $save ? $this->db->insert_id() : false;
    }

    public function find($id = '') {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $find = $this->db->get($this->table);
        return $find->row();
    }

    public function find_by_question_id($id = '') {
        $this->db->where('question_id', $id);
        $find = $this->db->get($this->table);
        return $find->result();
    }

    public function previous_id($evaluation_id = 0, $question_id = 0) {
        if (empty($question_id)) {
            return FALSE;
        }
        $this->db->select('q.id');
        $this->db->from($this->table . ' as q');
        $this->db->join('examinations as e', "e.question_id = q.id");
        $this->db->where('e.evaluation_id', $evaluation_id);
        $this->db->where('q.id <', $question_id);
        $this->db->order_by('q.id', 'asc');
        $this->db->limit(1);
        $find = $this->db->get()->row();
        if (!empty($find)) {
            return $find->id;
        }
        return FALSE;
    }

    public function first($evaluation_id = '') {
        $this->db->select('q.*');
        $this->db->from($this->table . ' as q');
        $this->db->join('examinations as e', "e.question_id = q.id");
        $this->db->where('e.evaluation_id', $evaluation_id);
        $this->db->order_by('q.id', 'ASC');
        $this->db->limit(1);
        $find = $this->db->get();
        return $find->row();
    }

    public function next_id($evaluation_id = 0, $question_id = 0) {
        if (empty($question_id)) {
            return FALSE;
        }
        $this->db->select('q.id');
        $this->db->from($this->table . ' as q');
        $this->db->join('examinations as e', "e.question_id = q.id");
        $this->db->where('e.evaluation_id', $evaluation_id);
        $this->db->where('q.id >', $question_id);
        $this->db->order_by('q.id', 'asc');
        $this->db->limit(1);
        $find = $this->db->get()->row();
        if (!empty($find)) {
            return $find->id;
        }
        return FALSE;
    }

    public function active($evaluation_id, $count_all_results = false) {
        $this->db->select('q.*');
        $this->db->from($this->table . ' as q');
        $this->db->join('examinations as e', 'e.question_id = q.id');
        $this->db->where('e.evaluation_id', $evaluation_id);
        if ($count_all_results) {
            $total = $this->db->count_all_results();
            return !empty($total) ? $total : 0 ;
        }
        return $this->db->get()->result();
    }

    public function edit($id = '', $data = array()) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function remove($id = '') {
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }

}
