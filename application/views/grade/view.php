<div class="row">
    <div class="col-md-12">
        <h3><?php echo $evaluation_title; ?></h3>
        <table class="table table-bordered table-condensed table-hover table-responsive table-striped">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Nama</td>
                    <td>Nilai</td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($grades as $grade) {
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo $grade->username; ?></td>
                        <td><?php echo $grade->total; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>