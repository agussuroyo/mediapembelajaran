<div class="row">
    <div class="col-md-12">
        <?php echo $pagination; ?>
        <table class="table table-bordered table-condensed table-hover table-responsive table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($evaluations as $evaluation) {
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo $evaluation->title; ?></td>
                        <td>
                            <a href="<?php echo site_url('grade/view/' . $evaluation->id); ?>" class="btn btn-default">Lihat Nilai</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php echo $pagination; ?>
    </div>
</div>