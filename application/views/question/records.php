<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('evaluation/records/'); ?>" class="btn btn-primary btn-sm pull-left">Daftar Evaluasi</a>
        <a href="<?php echo site_url('question/add/'); ?>" class="btn btn-primary btn-sm pull-right">Tambah Soal</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="text-justify">Daftar Soal</p>
        <?php echo $pagination; ?>
        <table class="table table-bordered table-condensed table-responsive table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Soal</th>
                    <th>Bobot</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($questions as $question) {
                    ?>
                    <tr>
                        <td><?php echo $start_number; ?></td>
                        <td>
                            <?php
                            echo $question->content;
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $question->weight;
                            ?>
                        </td>
                        <td>
                            <a href="<?php echo site_url('question/edit/' . $question->id); ?>" class="btn btn-sm btn-info">Ubah</a>
                            <a href="<?php echo site_url('question/remove/' . $question->id); ?>" class="btn btn-sm btn-danger">Hapus</a>
                        </td>
                    </tr>
                    <?php
                    $start_number++;
                }
                ?>
            </tbody>
        </table>
        <?php echo $pagination; ?>
    </div>
</div>