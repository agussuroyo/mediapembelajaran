<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('question/records'); ?>" class="btn btn-default btn-sm pull-left">Daftar Soal</a>
        <a href="<?php echo site_url('question/add'); ?>" class="btn btn-default btn-sm pull-right">Tambah Soal</a>
    </div>    
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        $this->load->view('question/_form');
        ?>
    </div>    
</div>

