<form accept-charset="utf-8" action="" method="post">
    <?php
    echo $error_message;
    ?>
    <div class="form-group">
        <label>Soal</label>
        <textarea class="form-control tinymce-editor" name="content" rows="10"><?php echo set_value('content', $question->content); ?></textarea>
    </div>
    <div class="form-group">
        <label>Bobot</label>
        <input type="text" value="<?php echo set_value('weight', $question->weight); ?>" name="weight" class="form-control" />
    </div>
    <div class="form-group">
        <label>Pilihan</label>
        <?php
        $range = range('a', 'd');
        $key = NULL;
        if (isset($answers) && !empty($answers)) {
            $lable = 'a';
            foreach ($answers as $k => $answer) {
                if($answer->key == 1){
                    $key = $k;
                }
                echo '<div class="row">';
                echo '<div class="col-md-1">';
                echo "<p clas='text-center'><strong>{$lable}.</strong></p>";
                echo '</div>';
                echo '<div class="col-md-11">';
                echo "<p clas='text-center'></p>";
                $this->load->view('question/_option', array('value' => set_value("option[{$k}]", $answer->content), 'name' => "option[{$k}]"));
                echo form_hidden("answer_id[{$k}]", $answer->id);
                echo '</div>';
                echo '</div>';
                $lable++;
            }
        } else {            
            foreach ($range as $k => $option) {
                echo '<div class="row">';
                echo '<div class="col-md-1">';
                echo "<p clas='text-center'><strong>{$option}.</strong></p>";
                echo '</div>';
                echo '<div class="col-md-11">';
                echo "<p clas='text-center'></p>";
                $this->load->view('question/_option', array('value' => set_value("option[{$k}]"), 'name' => "option[{$k}]"));
                echo '</div>';
                echo '</div>';
            }
        }
        ?>
    </div>
    <div class="form-group">
        <label>Kunci</label>
        <?php
        echo form_dropdown(array('name' => 'key'), $range, set_value('key',$key), array('class' => 'form-control'));
        ?>
    </div>
    <div class="form-group">
        <button type="submit" name="save" value="1" class="btn btn-primary btn-block">Simpan</button>
    </div>
</form>