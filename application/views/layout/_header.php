<?php
if ($this->session->userdata('is_logged_in')) {
    ?>
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>">
                            <i class="glyphicon glyphicon-cloud"></i>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-menu-navbar-collapse" aria-expanded="false"> 
                            <span class="sr-only">Toggle navigation</span> 
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span> 
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-menu-navbar-collapse">
                        <p class="navbar-text navbar-right">Hello, <a href="<?php echo site_url('user/profile'); ?>" class="navbar-link"><?php echo $username; ?></a></p>
                        <ul class="nav navbar-nav navbar-right">
                            <li><?php echo anchor('user/logout', 'Keluar'); ?></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo site_url(); ?>">
                            <i class="glyphicon glyphicon-cloud"></i>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <?php
}

