<div class="row">
    <div class="col-md-12">
        <?php
        echo $error_message;
        ?>
        <form accesskey="utf-8" action="" method="post">
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" value="<?php echo set_value('name', $user->name); ?>" name="name" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <label>NIM</label>
                <input type="text" value="<?php echo set_value('nim', $user->nim); ?>" name="nim" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" value="<?php echo set_value('email', $user->email); ?>" name="email" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <label>Nama Pengguna</label>
                <input type="text" value="<?php echo set_value('username', $user->username); ?>" name="username" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <label>Kata Sandi</label>
                <input type="password" value="<?php echo set_value('password'); ?>" name="password" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <label>Ulangi Kata Sandi</label>
                <input type="password" value="<?php echo set_value('repassword'); ?>" name="repassword" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-primary" name="save" value="1">Save</button>
            </div>
        </form>
    </div>
</div>