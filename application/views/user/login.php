<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->session->flashdata('login_errors');
        ?>
        <form action="" method="post" class="form">
            <div class="form-group">
                <input type="text" name="username" value="<?php echo set_value('username'); ?>" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <input type="password" name="password" value="<?php echo set_value('password'); ?>" class="form-control" autocomplete="off" />
            </div>
            <div class="form-group">
                <button type="submit" value="1" name="login" class="btn btn-default">Sign In</button>
            </div>
        </form>
        <?php
        echo anchor('user/forgot', 'Ganti Password?');
        echo ' atau ';
        echo anchor('user/register', 'Buat Akun');
        ?>
    </div>
</div>