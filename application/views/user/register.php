<div class="row">
    <div class="col-md-12">
        <?php echo $error_message; ?>
        <form accept-charset="utf-8" action="" method="post">
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" value="<?php echo set_value('name'); ?>" name="name" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <label>NIM</label>
                <input type="text" value="<?php echo set_value('nim'); ?>" name="nim" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" value="<?php echo set_value('email'); ?>" name="email" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <label>Nama Pengguna</label>
                <input type="text" value="<?php echo set_value('username'); ?>" name="username" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <label>Kata Sandi</label>
                <input type="password" value="<?php echo set_value('password'); ?>" name="password" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <label>Verifikasi Kata Sandi</label>
                <input type="password" value="<?php echo set_value('re_password'); ?>" name="re_password" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <button type="submit" name="save" value="1" class="btn btn-primary btn-block">Simpan</button>
            </div>
        </form>
    </div>
</div>