<?php
echo validation_errors();
echo $this->session->flashdata('reset_error');
?>
<form action="" method="post">
    <div class="form-group">
        <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" autocomplete="off" placeholder="Email" />
    </div>
    <div class="form-group">
        <button type="submit" name="forgot" value="1" class="btn btn-primary">Submit</button>
    </div>
</form>