<div class="row">
    <div class="col-md-12">
        <form accept-charset="utf-8" action="" method="post">
            <div class="form-group">
                <label>Pilih Evaluasi Aktif</label>
                <select class="form-control" name="active_evaluation">
                    <option value="">Tidak Ada</option>
                    <?php
                    foreach ($evaluations_list as $eval) {
                        ?>
                        <option value="<?php echo $eval->id; ?>" <?php echo set_select('active_evaluation', $eval->id, $evaluation_active == $eval->id); ?>><?php echo $eval->title; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <button name="set_active_evaluation" type="submit" value="1" class="btn btn-primary btn-sm">Simpan</button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('question/records/'); ?>" class="btn btn-primary btn-sm pull-left">Daftar Soal</a>
        <a href="<?php echo site_url('evaluation/add/'); ?>" class="btn btn-primary btn-sm pull-right">Tambah Evaluasi</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="text-justify">Daftar Evaluasi</p>
        <?php echo $pagination; ?>
        <table class="table table-bordered table-condensed table-responsive table-striped">
            <thead>
                <tr>
                    <th>Judul</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($evaluations as $evaluation) {
                    ?>
                    <tr>
                        <td><?php echo $evaluation->title; ?></td>
                        <td><?php echo $evaluation->status == 1 ? 'Aktif' : 'Tidak Aktif'; ?></td>
                        <td>
                            <a href="<?php echo site_url('evaluation/edit/' . $evaluation->id); ?>" class="btn btn-default btn-xs">Edit</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php echo $pagination; ?>
    </div>
</div>