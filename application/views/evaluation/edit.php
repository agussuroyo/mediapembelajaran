<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('evaluation/records/'); ?>" class="btn btn-default btn-sm pull-left">Daftar Evaluasi</a>
        <a href="<?php echo site_url('evaluation/add/'); ?>" class="btn btn-default btn-sm pull-right">Tambah Evaluasi</a>
    </div>
</div><div class="row">
    <div class="col-md-12">
        <?php
        $this->load->view('evaluation/_form');
        ?>
    </div>
</div>