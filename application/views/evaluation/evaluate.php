<div class="row">
    <div class="col-md-6">
        <div class="pull-left">
            <p class="text-capitalize">Total Pertanyaan: <strong><?php echo $total; ?></strong></p>
            <p class="text-capitalize">Bobot: <strong><?php echo $question->weight; ?></strong></p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="pull-right">
            <p class="text-capitalize">Total Terjawab: <strong><?php echo $total_submited; ?></strong></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form accept-charset="utf-8" action="" method="post">
            <?php
            echo $validation_errors;
            ?>
            <ul class="list-group">
                <li class="list-group-item"><?php echo $question->content; ?></li>
            </ul>
            <ul class="list-group">
                <?php
                foreach ($answers as $answer) {
                    ?>
                    <li class="list-group-item">
                        <?php
                        echo form_radio('answer', $answer->id, ($answer->id == get_cookie('answer_question_' . $question->id)));
                        echo $answer->content;
                        ?>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <ul class="list-group">
                <li class="list-group-item">
                    <button type="submit" name="save" value="1" class="btn btn-primary btn-sm btn-block">Submit</button>
                </li>
            </ul>            
            <?php
            echo $previous;
            echo $next;
            ?>
        </form>
    </div>
</div>