<?php
echo $error_message;
?>
<form action="" method="post" accept-charset="utf-8" class="form">    
    <div class="form-group">
        <label>Judul Evaluasi</label>
        <input type="text" value="<?php echo set_value('title', $evaluation->title); ?>" name="title" class="form-control" autocomplete="off" />
    </div>
    <div class="form-group">
        <label>Pilih Soal-soal Evaluasi</label>
        <div class="row">
            <?php
            if (isset($examinations)) {
                foreach ($questions as $key => $question) {
                    ?>
                    <div class="col-md-3">
                        <input type="checkbox" name="question[<?php echo $key; ?>]" value="<?php echo $question->id; ?>" <?php echo set_checkbox("question[{$key}]", $question->id, in_array($question->id, $examinations)) ?> /> <?php echo $question->content; ?>
                    </div>
                    <?php
                }
            } else {
                foreach ($questions as $question) {
                    ?>
                    <div class="col-md-3">
                        <input type="checkbox" name="question[]" value="<?php echo $question->id; ?>" /> <?php echo $question->content; ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" value="1" name="save" class="btn btn-primary">Simpan</button>
    </div>
</form>