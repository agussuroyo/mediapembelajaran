<?php
echo $this->session->flashdata('save_error');
?>
<form accept-charset="utf-8" action="" method="post">
    <div class="form-group">
        <label>Judul</label>
        <input type="text" name="title" value="<?php echo set_value('title', $materi->title); ?>" autocomplete="off" class="form-control" />
    </div>
    <div class="form-group">
        <label>Materi</label>
        <textarea name="content" class="form-control tinymce-editor" rows="10"><?php echo set_value('content', $materi->content); ?></textarea>
    </div>
    <div class="form-group">
        <button type="submit" value="1" name="save" class="btn btn-primary">Save</button>
    </div>
</form>
