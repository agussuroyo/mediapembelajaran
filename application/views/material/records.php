<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('material/add/'); ?>" class="btn btn-primary btn-sm pull-right">Tambah Materi</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="text-justify">Daftar Materi</p>
        <?php
        echo $pagination;
        ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php
            foreach ($materials as $matery) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading-<?php echo $matery->id; ?>">
                        <h4 class="panel-title">
                            <a role="button" class="" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $matery->id; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $matery->id; ?>">
                                <?php echo $matery->title; ?>
                            </a>
                            <a role="button" class="btn btn-default btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $matery->id; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $matery->id; ?>">
                               Lihat
                            </a>
                            <a href="<?php echo site_url('material/edit/' . $matery->id); ?>" role="button" class="btn btn-info btn-xs">Ubah</a>
                            <a href="<?php echo site_url('material/edit/' . $matery->id); ?>"  role="button" class="btn btn-danger btn-xs pull-right">Hapus</a>
                        </h4>
                    </div>
                    <div id="collapse-<?php echo $matery->id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $matery->id; ?>">
                        <div class="panel-body">
                            <?php echo $matery->content; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        echo $pagination;
        ?>
    </div>
</div>

