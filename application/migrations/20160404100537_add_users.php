<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_users extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'nim' => array(
                'type' => 'INT',
                'constraint' => 12,
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'type' => array(
                'type' => 'INT',
                'constraint' => 2,
                'default' => 0
            ),
            'created_at' => array(
                'type' => 'DATETIME'
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('users');
        $this->db->insert('users', array('username' => 'admin', 'password' => md5('admin'), 'type' => 1));
        $this->db->insert('users', array('username' => 'student', 'password' => md5('student'), 'type' => 0));
    }

    public function down() {
        $this->dbforge->drop_table('users');
    }

}
