<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_answers extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'author' => array(
                'type' => 'BIGINT',
                'constraint' => 20
            ),
            'question_id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
            ),
            'lable' => array(
                'type' => 'VARCHAR',
                'constraint' => 1,
            ),
            'content' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'key' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ),
            'created_at' => array(
                'type' => 'DATETIME'
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('answers');
    }

    public function down() {
        $this->dbforge->drop_table('answers');
    }

}
