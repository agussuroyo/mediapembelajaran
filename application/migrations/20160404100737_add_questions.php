<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_questions extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'author' => array(
                'type' => 'BIGINT',
                'constraint' => 20
            ),
            'content' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'weight' => array(
                'type' => 'INT',
                'constraint' => 2,
                'default' => 0
            ),
            'created_at' => array(
                'type' => 'DATETIME'
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('questions');
    }

    public function down() {
        $this->dbforge->drop_table('questions');
    }

}
