<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_grades extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'BIGINT',
                'constraint' => 20
            ),
            'total' => array(
                'type' => 'INT',
                'constraint' => 12,
                'default' => 0
            ),
            'evaluation_id' => array(
                'type' => 'INT',
                'constraint' => 12,
                'default' => 0
            ),
            'created_at' => array(
                'type' => 'DATETIME'
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('grades');
    }

    public function down() {
        $this->dbforge->drop_table('grades');
    }

}
