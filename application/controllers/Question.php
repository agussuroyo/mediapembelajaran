<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function records($offset = 0) {
        // Prepare extension for operation
        $this->load->library(array('session', 'form_validation', 'pagination'));
        $this->load->helper(array('form', 'url'));
        $this->load->model('questions');

        $per_page = 10;
        $total = $this->questions->get_all(null, null, true);
        $config['base_url'] = site_url('question/records/');
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['start_number'] = $offset < 1 ? 1 : ceil($total / $offset);
        $data['total'] = $total;
        $data['pagination'] = $this->pagination->create_links();
        $data['yield'] = 'question/records';
        $data['username'] = $this->session->userdata('username');
        $data['questions'] = $this->questions->get_all($per_page, $offset);
        $this->load->view('layout/dashboard', $data);
    }

    public function add() {
        // Prepare required extension for operation
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url', 'model'));

        // Procesing save data
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('content', 'Soal', 'required', array('required' => '{field} tidak boleh kosong.'));
            $this->form_validation->set_rules('weight', 'Bobot', 'required|numeric', array('required' => '{field} tidak boleh kosong.', 'numeric' => '{field} harus berupa angka.'));
            $this->form_validation->set_rules('key', 'Kunci', 'required', array('required' => '{field} tidak boleh kosong.'));
            if ($this->form_validation->run() == true) {
                $this->load->model(array('questions', 'answers'));
                $question['content'] = $this->input->post('content');
                $question['created_at'] = date('Y-m-d H:i:s');
                $question['weight'] = $this->input->post('weight');
                $question_id = $this->questions->add($question);
                if ($question_id) {
                    $options = $this->input->post('option');
                    $author = $this->session->userdata('id');
                    $key = $this->input->post('key');
                    foreach ($options as $k => $option) {
                        $answer = array();
                        $answer['created_at'] = date('Y-m-d H:i:s');
                        $answer['author'] = $author;
                        $answer['question_id'] = $question_id;
                        $answer['content'] = $option;
                        $answer['key'] = ($k == $key) ? 1 : 0;
                        $this->answers->add($answer);
                    }
                }
                redirect('question/edit/' . $question_id);
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'question/add';
        $data['question'] = model_fields('questions');
        $this->load->view('layout/dashboard', $data);
    }

    public function edit($question_id = '') {
        // Prepare required extension for operation
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('questions', 'answers'));

        // Procesing save data
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('content', 'Soal', 'required', array('required' => '{field} tidak boleh kosong.'));
            $this->form_validation->set_rules('weight', 'Bobot', 'required|numeric', array('required' => '{field} tidak boleh kosong.', 'numeric' => '{field} harus berupa angka.'));
            $this->form_validation->set_rules('key', 'Kunci', 'required', array('required' => '{field} tidak boleh kosong.'));
            if ($this->form_validation->run() == true) {
                $question['content'] = $this->input->post('content');
                $question['updated_at'] = date('Y-m-d H:i:s');
                $question['weight'] = $this->input->post('weight');
                $save = $this->questions->edit($question_id, $question);
                if ($save) {
                    $answers = $this->input->post('answer_id');
                    $key = $this->input->post('key');
                    foreach ($answers as $order => $answer_id) {
                        $answer = array();
                        $answer['created_at'] = date('Y-m-d H:i:s');
                        $answer['question_id'] = $question_id;
                        $answer['content'] = $this->input->post("option[{$order}]");
                        $answer['key'] = ($order == $key) ? 1 : 0;
                        $this->answers->edit($answer_id, $answer);
                    }
                }
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'question/edit';
        $data['question'] = $this->questions->find($question_id);
        $data['answers'] = $this->answers->find_by_question_id($question_id);
        $this->load->view('layout/dashboard', $data);
    }

    public function remove($question_id = '') {
        $this->load->helper('url');
        $this->load->model('questions');
        $this->questions->remove($question_id);
        redirect('question/records/');
    }

}
