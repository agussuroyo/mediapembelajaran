<?php

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('session');
        $this->load->helper('url');
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/index';
        $this->load->view('layout/welcome', $data);
    }

    public function student() {
        $this->load->library('session');
        $this->load->helper('url');
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/student';
        $this->load->view('layout/welcome', $data);
    }

    public function lecture() {
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('form', 'url'));
        
        // Check if already logged in
        if ($this->session->userdata('is_logged_in')) {
            redirect('dashboard');
        }

        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $this->load->model('users');
            $user = $this->users->valid_user($username, $password);
            if ($user) {
                $user_data = array(
                    'is_logged_in' => TRUE,
                    'id' => $user->id,
                    'type' => $user->type,
                    'username' => $user->username
                );
                $this->session->set_userdata($user_data);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('login_errors', 'Username or Password Incorrect.');
            }
        }
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/lecture';
        $this->load->view('layout/welcome', $data);
    }

    public function login() {
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('form', 'url'));

        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $this->load->model('users');
            $user = $this->users->valid_user($username, $password);
            if ($user) {
                $user_data = array(
                    'is_logged_in' => TRUE,
                    'id' => $user->id,
                    'type' => $user->type,
                    'username' => $user->username
                );
                $this->session->set_userdata($user_data);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('login_errors', 'Username or Password Incorrect.');
            }
        }

        $data['yield'] = 'user/login';
        $this->load->view('layout/welcome', $data);
    }

    public function logout() {
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->unset_userdata('is_logged_in');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('type');
        $this->session->sess_destroy();
        redirect('user/index');
    }

    public function profile($user_id = '') {
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('users');
        if (empty($user_id)) {
            $user_id = $this->session->userdata('id');
        }
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Nama Pengguna', 'required', array('required' => '{field} tidak boleh kosong.'));
            $password = $this->input->post('password');
            if ($password) {
                $this->form_validation->set_rules('password', 'Kata Sandi', 'required', array('required' => '{field} tidak boleh kosong.'));
                $this->form_validation->set_rules('repassword', 'Verifikasi Kata Sandi', 'required|matches[password]', array('required' => '{field} tidak boleh kosong.', 'matches' => '{field} tidak sah.'));
            }
            if ($this->form_validation->run() == true) {
                $user = array();
                $user['updated_at'] = date('Y-m-d H:i:s');
                $user['username'] = $this->input->post('username');
                $user['name'] = $this->input->post('name');
                $user['nim'] = $this->input->post('nim');
                $user['email'] = $this->input->post('email');
                if ($password) {
                    $user['password'] = md5($password);
                }
                $this->users->edit($user_id, $user);
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['user'] = $this->users->find($user_id);
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/profile';
        $this->load->view('layout/dashboard', $data);
    }

    public function register() {
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('form', 'url'));
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
            $this->form_validation->set_message('numeric', '{field} harus berupa angka.');
            $this->form_validation->set_rules('name', 'Nama', 'trim|required');
            $this->form_validation->set_rules('nim', 'NIM', 'trim|required|numeric|callback_is_nim_exist[nim]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_is_email_exist[email]');
            $this->form_validation->set_rules('username', 'Nama Pengguna', 'trim|required|callback_is_username_exist[username]');
            $this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required');
            $this->form_validation->set_rules('re_password', 'Verifikasi Kata Sandi', 'trim|required|matches[password]');
            if ($this->form_validation->run() == true) {
                $this->load->model('users');
                $user = array();
                $user['name'] = $this->input->post('name');
                $user['nim'] = $this->input->post('nim');
                $user['email'] = $this->input->post('email');
                $user['username'] = $this->input->post('username');
                $user['password'] = md5($this->input->post('password'));
                $user['type'] = 0;
                $user['created_at'] = date('Y-m-d H:i:s');
                $this->users->add($user);
                redirect(site_url('user/login'));
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/register';
        $this->load->view('layout/welcome', $data);
    }

    public function register_lecture() {
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('form', 'url'));
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
            $this->form_validation->set_message('numeric', '{field} harus berupa angka.');
            $this->form_validation->set_rules('name', 'Nama', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_is_email_exist[email]');
            $this->form_validation->set_rules('username', 'Nama Pengguna', 'trim|required|callback_is_username_exist[username]');
            $this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required');
            $this->form_validation->set_rules('re_password', 'Verifikasi Kata Sandi', 'trim|required|matches[password]');
            if ($this->form_validation->run() == true) {
                $this->load->model('users');
                $user = array();
                $user['name'] = $this->input->post('name');
                $user['email'] = $this->input->post('email');
                $user['username'] = $this->input->post('username');
                $user['password'] = md5($this->input->post('password'));
                $user['type'] = 1;
                $user['created_at'] = date('Y-m-d H:i:s');
                $this->users->add($user);
                redirect(site_url('user/login'));
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'user/register_lecture';
        $this->load->view('layout/welcome', $data);
    }

    public function is_username_exist($value, $field) {
        $this->load->model('users');
        if ($this->users->is_exists($field, $value)) {
            $this->form_validation->set_message("is_username_exist", '{field} sudah ada.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function is_email_exist($value, $field) {
        $this->load->model('users');
        if ($this->users->is_exists($field, $value)) {
            $this->form_validation->set_message("is_email_exist", '{field} sudah ada.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function is_nim_exist($value, $field) {
        $this->load->model('users');
        if ($this->users->is_exists($field, $value)) {
            $this->form_validation->set_message("is_nim_exist", '{field} sudah ada.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function forgot() {
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('form', 'url'));
        if ($this->input->post('forgot')) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == TRUE) {
                $this->load->model('users');
                $email = $this->input->post('email', true);
                $is_exist = $this->users->email_exists($email);
                if ($is_exist) {
                    $password = $this->user->generate_password($email, true);
                    $this->load->library('email');

                    $this->email->from('study@elearning.com', 'E-Learning');
                    $this->email->to($email);
                    $this->email->subject('E-Learning Password Reset');
                    $this->email->message('New Password = ' . $password);

                    $this->email->send();
                    $this->session->set_flashdata('reset_error', 'New Password has been sent to your email.');
                } else {
                    $this->session->set_flashdata('reset_error', 'Email not exist.');
                }
            }
        }
        $data['yield'] = 'user/forgot';
        $this->load->view('layout/welcome', $data);
    }

}
