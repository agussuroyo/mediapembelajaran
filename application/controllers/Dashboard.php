<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('user/login');
        }
        //Do your magic here
    }

    public function index() {
        $this->load->model('evaluations');
        $data['evaluation_active'] = $this->evaluations->get_active_id();
        $data['user_type'] = $this->session->userdata('type');
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'dashboard/index';
        $this->load->view('layout/dashboard', $data);
    }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */