<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Grade extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('account');
        }
    }

    public function records($offset = 0) {
        $this->load->library('pagination');
        $this->load->model(array('grades', 'evaluations'));
        $data['username'] = $this->session->userdata('username');
        $per_page = 10;
        $total = $this->evaluations->all(null, null, true);
        $config['base_url'] = site_url('grade/records/');
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['evaluations'] = $this->evaluations->all($per_page, $offset);
        $data['grades'] = $this->grades->all(null, null, false);
        $data['yield'] = 'grade/records';
        $this->load->view('layout/dashboard', $data);
    }

    public function view($id = '') {
        $this->load->library('pagination');
        $this->load->model(array('grades', 'evaluations'));
        $data['evaluation_title'] = $this->evaluations->find($id)->title;
        $data['grades'] = $this->grades->find_by_evaluation_id($id);
        $data['yield'] = 'grade/view';
        $data['username'] = $this->session->userdata('username');
        $this->load->view('layout/dashboard', $data);
    }

}
