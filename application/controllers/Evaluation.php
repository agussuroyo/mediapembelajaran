<?php

class Evaluation extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function records($offset = 0) {
        $this->load->library(array('session', 'form_validation', 'pagination'));
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('evaluations', 'questions'));

        // Set Active Evaluation
        if ($this->input->post('set_active_evaluation')) {
            $evaluation_id = $this->input->post('active_evaluation');
            $this->evaluations->deactivate_all();
            if (!empty($evaluation_id)) {
                $this->evaluations->edit($evaluation_id, array('status' => 1));
            }
        }

        $per_page = 10;
        $total = $this->evaluations->all(null, null, true);
        $config['base_url'] = site_url('evaluation/records/');
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['evaluations'] = $this->evaluations->all($per_page, $offset);
        $data['evaluations_list'] = $this->evaluations->all(null, null);
        $data['evaluation_active'] = $this->evaluations->get_active_id();
        $data['pagination'] = $this->pagination->create_links();
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'evaluation/records';
        $this->load->view('layout/dashboard', $data);
    }

    public function add() {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url', 'model'));
        $this->load->model(array('evaluations', 'questions'));
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('title', 'Judul', 'required', array('required' => '{field} tidak boleh kosong.'));
            if ($this->form_validation->run() == true) {
                $evaluation_id = $this->evaluations->add(array('title' => $this->input->post('title'), 'status' => 0, 'user_id' => $this->session->userdata('id')));
                if ($evaluation_id) {
                    $this->load->model('examinations');
                    foreach ($this->input->post('question') as $question) {
                        $this->examinations->add(array('question_id' => $question, 'evaluation_id' => $evaluation_id));
                    }
                    redirect('evaluation/edit/' . $evaluation_id);
                }
            } else {
                $data['error_message'] = validation_errors();
            }
        }

        $data['username'] = $this->session->userdata('username');
        $data['questions'] = $this->questions->get_all(null, null, false);
        $data['evaluation'] = model_fields('evaluations');
        $data['yield'] = 'evaluation/add';
        $this->load->view('layout/dashboard', $data);
    }

    public function edit($id = '') {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url', 'model'));
        $this->load->model(array('evaluations', 'questions', 'examinations'));
        $data['error_message'] = '';
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('title', 'Judul', 'required', array('required' => '{field} tidak boleh kosong.'));
            if ($this->form_validation->run() == true) {
                $edit = $this->evaluations->edit($id, array('title' => $this->input->post('title')));
                if ($edit) {
                    $this->examinations->remove_by_evaluation_id($id);
                    foreach ($this->input->post('question') as $question) {
                        $this->examinations->add(array('question_id' => $question, 'evaluation_id' => $id));
                    }
                }
            } else {
                $data['error_message'] = validation_errors();
            }
        }
        $data['username'] = $this->session->userdata('username');
        $data['questions'] = $this->questions->get_all(null, null, false);
        $exams = $this->examinations->find_by_evaluation_id($id);
        $examinations = array();
        foreach ($exams as $ex) {
            $examinations[] = $ex->question_id;
        }
        $data['examinations'] = $examinations;
        $data['evaluation'] = $this->evaluations->find($id);
        $data['yield'] = 'evaluation/edit';
        $this->load->view('layout/dashboard', $data);
    }

    public function remove($id = '') {
        $this->load->library('session');
        $this->load->helper('url');
        redirect('evaluation/recordss');
    }

    public function evaluate($evaluation_id = 0, $question_id = 0) {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('url', 'form', 'cookie'));
        $this->load->model(array('evaluations', 'questions', 'answers'));
        // Redirect when not login
        if (!$this->session->userdata('is_logged_in')) {
            redirect('user/login');
        }
        // Check available evaluations
        if ($this->evaluations->is_available()) {

            // Declare Question ID & default data
            if ($question_id < 1) {
                $question = $this->questions->first($evaluation_id);
                if ($question) {
                    $question_id = $question->id;
                }
            } else {
                $question = $this->questions->find($question_id);
            }
            $data['question'] = $question;
            $data['validation_errors'] = '';

            // Get Next & Previous Question
            $previous_id = $this->questions->previous_id($evaluation_id, $question_id);
            $next_id = $this->questions->next_id($evaluation_id, $question_id);

            // Process when Submit the Answer
            if ($this->input->post('save')) {
                $this->form_validation->set_rules('answer', 'Jawaban', 'required', array('required' => '{field} tidak boleh kosong.'));
                if ($this->form_validation->run() == true) {
                    set_cookie('answer_question_' . $question_id, $this->input->post('answer'), 3600 * 24);
                    if ($next_id) {
                        redirect('evaluation/evaluate/' . $evaluation_id . '/' . $next_id);
                    } else {
                        redirect(current_url());
                    }
                } else {
                    $data['validation_errors'] = validation_errors();
                }
            }

            // Apply link for next and previous answer
            $data['previous'] = $previous_id ? anchor('evaluation/evaluate/' . $evaluation_id . '/' . $previous_id, 'Sebelumnya', array('class' => 'btn btn-info btn-sm pull-left')) : '';
            $data['next'] = $next_id ? anchor('evaluation/evaluate/' . $evaluation_id . '/' . $next_id, 'Selanjutnya', array('class' => 'btn btn-info btn-sm pull-right')) : '';
            $data['answers'] = $this->answers->find_by_question_id($question_id);
            $data['total'] = $total = $this->questions->active($evaluation_id, true);

            // Calculate Number of Answer
            $total_submited = 0;
            $active = $this->questions->active($evaluation_id);
            foreach ($active as $question) {
                $submited = get_cookie('answer_question_' . $question->id);
                if ($submited) {
                    $total_submited++;
                }
            }

            $grade = 0;
            if ($total == $total_submited) {
                $keys = $this->questions->get_keys();
                foreach ($keys as $key) {
                    $answer = get_cookie('answer_question_' . $key->question);
                    if ($answer == $key->answer) {
                        $grade += $key->weight;
                    }
                }
            }
            $data['grade'] = $grade;
            
            // Finishing when submit all question
            if ($this->input->post('end') && ($total == $total_submited)) {
                $this->load->model('grades');
                $save_grade = array();
                $save_grade['user_id'] = $this->session->userdata('id');
                $save_grade['total'] = $grade;
                $save_grade['evaluation_id'] = $evaluation_id;
                $save_grade['created_at'] = date('Y-m-d H:i:s');
                $this->grades->add($save_grade);
                foreach ($active as $question) {
                    delete_cookie('answer_question_' . $question->id);
                }
                redirect('dashboard');
            }

            // Displaying all generated data            
            $data['total_submited'] = $total_submited;
            $data['yield'] = ($total == $total_submited) ? 'evaluation/grade' : 'evaluation/evaluate';
        } else {
            $data['yield'] = 'evaluation/empty';
        }
        $data['username'] = $this->session->userdata('username');
        $this->load->view('layout/dashboard', $data);
    }

}
