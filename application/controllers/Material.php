<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Material extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('materials');
        $data['yield'] = 'material/index';
        $data['materials'] = $this->materials->get_all();
        $data['username'] = $this->session->userdata('username');
        $this->load->view('layout/welcome', $data);
    }

    public function add() {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url', 'model'));
        $this->form_validation->set_rules('title', 'Judul', 'trim|required');
        $this->form_validation->set_rules('content', 'Isi Materi', 'trim');
        if ($this->form_validation->run() == true) {
            $this->load->model('materials');
            $material['title'] = $this->input->post('title', true);
            $material['content'] = $this->input->post('content', true);
            $material['created_at'] = date('Y-m-d H:i:s');
            $materi_id = $this->materials->add($material);
            if ($materi_id) {
                redirect('material/edit/' . $materi_id);
            } else {
                $this->session->set_flashdata('save_error', 'Saving problem appear, please try again.');
            }
        } else {
            $this->session->set_flashdata('save_error', validation_errors());
        }
        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'material/add';
        $data['materi'] = model_fields('materials');
        $this->load->view('layout/dashboard', $data);
    }

    public function edit($id = '') {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->model('materials');

        $this->form_validation->set_rules('title', 'Judul', 'trim|required');
        $this->form_validation->set_rules('content', 'Isi Materi', 'trim');
        if ($this->form_validation->run() == true) {
            $this->load->model('materials');
            $material['title'] = $this->input->post('title', true);
            $material['content'] = $this->input->post('content', true);
            $material['updated_at'] = date('Y-m-d H:i:s');
            $this->materials->edit($id, $material);
            $this->session->set_flashdata('save_success', 'Saving problem appear, please try again.');
        } else {
            $this->session->set_flashdata('save_error', validation_errors());
        }

        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'material/edit';
        $data['materi'] = $this->materials->find($id);
        $this->load->view('layout/dashboard', $data);
    }

    public function view($id = '') {
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->model('materials');

        $data['username'] = $this->session->userdata('username');
        $data['yield'] = 'material/view';
        $data['materials'] = $this->materials->get_all();
        $data['materi'] = $this->materials->find($id);
        $this->load->view('layout/dashboard', $data);
    }

    public function records($offset = 0) {
        $this->load->library(array('session', 'form_validation', 'pagination'));
        $this->load->helper(array('form', 'url'));
        $this->load->model('materials');

        $per_page = 10;
        $total = $this->materials->get_all(null, null, true);
        $config['base_url'] = site_url('material/records/');
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['pagination'] = $this->pagination->create_links();
        $data['yield'] = 'material/records';
        $data['materials'] = $this->materials->get_all($per_page, $offset);
        $data['username'] = $this->session->userdata('username');
        $this->load->view('layout/dashboard', $data);
    }

}
