<?php

if (!function_exists('model_fields')) {

    function model_fields($table = '') {
        $ci = &get_instance();
        $ci->load->database();
        $fields = $ci->db->list_fields($table);
        $object = new stdClass();
        foreach ($fields as $value) {
            $object->{$value} = NULL;
        }
        return $object;
    }

}